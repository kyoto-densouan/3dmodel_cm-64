# README #

1/3スケールのRoland CM-64風小物のstlファイルです。 スイッチ類、I/Oポート等は省略しています。 

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 元ファイルはAUTODESK 123D DESIGNです。 

***

# 実機情報

## メーカ
- ローランド

## 発売時期
- 1989年

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/LA%E9%9F%B3%E6%BA%90)
- [藤本健の “DTMステーション”](https://www.dtmstation.com/archives/51908422.html)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_cm-64/raw/d40286df3731b434effb3cb14b31ce0066d604b6/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_cm-64/raw/d40286df3731b434effb3cb14b31ce0066d604b6/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_cm-64/raw/d40286df3731b434effb3cb14b31ce0066d604b6/ExampleImage.png)
